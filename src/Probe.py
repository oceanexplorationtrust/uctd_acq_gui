#!/usr/bin/python

#The MIT License (MIT)
# 
#Copyright (c) 2016, Ocean Exploration Trust
#Copyright (c) 2016, J. Ian S. Vaughn
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import bluetooth
import time
import datetime
import sys
import math
import os.path
import yaml
import argparse

# Internal dependencies
import NGcore.QtLogger as Log

class Probe(object):
    def __init__(self, name, addr=None):
        self.devname = name
        self._address = addr
        self._sock = None
        self._port = 1 # The right answer for this probe.  We should really look for a serial port service

    def findAddress(self):
        ''' Look up the device's bluetooth address.  If we've already got it,
        exit quickly '''
        if self._address is not None:
            return

        nearby_devices = bluetooth.discover_devices()
        for baddr in nearby_devices:
            if self.devname == bluetooth.lookup_name( baddr ):
                self._address = baddr
                Log.log('[probe] Found bluetooth address \"%s\" matching our devname \"%s\"' % (baddr, self.devname), Log.VERB_ALERT)
                break

    def getAddress(self):
        return self._address

    def connect(self):
        self.findAddress()
        if self._address is None:
            Log.log('[probe] Connection failed because no bluetooth address was found (device not in discovery mode?)', Log.VERB_WARN)
            return

        if self._sock is not None:
            Log.log('[probe] Found open socket to probe, attempting to re-use', Log.VERB_ALERT)
            # Check for a timeout by getting the ds string
            try:
                self.getStatus()
                return
            except bluetooth.btcommon.BluetoothError:
                Log.log('[probe] Error while attempting to re-use the existing bluetooth interface.  Probably a sign we need to re-initialize!', Log.VERB_ALERT)
                Log.logException('[probe]', Log.VERB_DEBUG)

        try:
            self._sock = bluetooth.BluetoothSocket (bluetooth.RFCOMM)
            self._sock.connect((self._address, self._port))
            # receive any garbage before a command prompt
            self._recv_to_prompt()
        except bluetooth.btcommon.BluetoothError:
            Log.log('[probe] Error while attempting to connect to probe.  It probably isn\'t available!', Log.VERB_ALERT)
            Log.logException('[probe]', Log.VERB_DEBUG)
            raise Exception('Unable to contact probe!')

    def _recv_to_prompt(self):
        buf = ''
        if self._sock is None:
            raise Exception('Uninitialized Socket')
        while True:
            buf += self._sock.recv(1024)
            if buf.endswith('S>'):
                return buf
        raise Exception('Connection timed out looking for prompt!')

    def recv_cmd_results(self):
        ''' Receive command results, stripping the first two lines'''
        buf = self._recv_to_prompt()
        # Cut out everything before the second newline
        idx1 = buf.find('\n')
        idx = buf.find('\n',idx1+1)+1
        return buf[idx:buf.rfind('\n')]

    def getStatusString(self):
        Log.log('[probe] Grabbing status string from probe', Log.VERB_DEBUG)
        if self._sock is None:
            raise Exception('Unintialized connection!')

        self._sock.send('ds\r\n')

        return self.recv_cmd_results()

    def getCal(self):
        Log.log('[probe] Grabbing probe calibration string from probe', Log.VERB_DEBUG)
        if self._sock is None:
            raise Exception('Unintialized connection!')

        self._sock.send('dcal\r\n')

        return self.recv_cmd_results()

    def getHdr(self, castId=None):
        Log.log('[probe] Grabbing cast header string', Log.VERB_DEBUG)
        if self._sock is None:
            raise Exception('Unintialized connection!')

        if castId == None:
            self._sock.send('gh\r\n')
        else:
            self._sock.send('gh=%d\r\n' % castId)

        return self.recv_cmd_results()

    def wipeProbe(self):
        Log.log('[probe] Wiping probe\'s internal memory...', Log.VERB_WARN)
        if self._sock is None:
            raise Exception('Unintialized connection!')
        self._sock.send('initlogging\r\n')
        return self.recv_cmd_results()

    def getStatus(self):
        Log.log('[probe] Grabbing probe status string', Log.VERB_DEBUG)
        buf = self.getStatusString()

        ret = {}
        for line in buf.split('\n'):
            idx = line.find('=')
            if idx is None:
                continue
            if line.startswith('SerialNumber='):
                ret['serial_number'] = line[idx+1:].strip()

            elif line.startswith('NumberCasts='):
                ret['num_casts'] = float(line[idx+1:].strip())

            elif line.startswith('SamplesFree='):
                ret['samples_free'] = float(line[idx+1:].strip())

            elif line.startswith('SamplesStored='):
                ret['samples_used'] = float(line[idx+1:].strip())

            elif line.startswith('MainBatteryVoltage='):
                ret['battery'] = float(line[idx+1:].strip())

            elif line.startswith('Stop seconds='):
                ret['stop_seconds'] = float(line[idx+1:].strip())

            elif line.startswith('DateTime='):
                #ret['time'] = line[idx+1:].strip()
                ret['time'] = datetime.datetime.strptime(line[idx+1:].strip(), '%d %b %Y %H:%M:%S')
        return ret


    def getCastList(self):
        ret = []
        Log.log('[probe] Syncing probe cast list', Log.VERB_DEBUG)

        hdr = self.getHdr()
        if not hdr or 'No casts stored' in hdr:
            return []
        for line in hdr.split('\n'):
            parts = line.strip().split()
            idnum = int(parts[1])
            castTime = datetime.datetime.strptime(' '.join(parts[2:6]), '%d %b %Y %H:%M:%S')
            ret.append({'time': castTime, 'idnum': idnum})

        return ret

            

    def getCast(self, castId):
        Log.log('[probe] Syncing probe cast id=%d' % castId, Log.VERB_INFO)

        if self._sock is None:
            raise Exception('Unintialized connection!')

        self._sock.send('gc=%d\r\n' % castId)

        return self.recv_cmd_results()

    def exportCast(self, castId, outfd):
        #outfd.write('*CAST_%s' % datetime.datetime.utcnow().strftime('%Y%m%d_%H%M%S\n'))
        Log.log('[probe] Exporting cast id=%d' % castId, Log.VERB_DEBUG)
        outfd.write('*CAST %d\n' % castId)
        outfd.write('*Lat\n')
        outfd.write('*Lon\n')
        calData = self.getCal()
        for line in calData.split('\n'):
            outfd.write('*%s\n' % line.strip())
        outfd.write('*sampling rate: 16 Hz\n')
        outfd.write('*scan# C[S/m]  T[degC]  P[dbar]\n')
        cast = self.getCast(castId)
        for line in cast.split('\n'):
            outfd.write('%s\n' % line.strip())

    def setTime(self):
        if self._sock is None:
            raise Exception('Unintialized connection!')

        Log.log('[probe] Setting probe time', Log.VERB_ALERT)

        self._sock.send('datetime=%s\r\n' % datetime.datetime.utcnow().strftime('%m%d%Y%H%M%S'))
        return self._recv_to_prompt()

if __name__ == '__main__':
    devname = 'SBE 07020194'
    uctd = Probe(devname)
    uctd.findAddress()
    uctd.connect()
    addr = uctd.getAddress()
    if addr is not None:
        print 'Probe found at address \"%s\", proceeding' % addr
        status = uctd.getStatus()
        print 'Serial #: %s' % status['serial_number']
        print 'Battery: %.2f' % status['battery']
        print 'Samples used: %d' % status['samples_used']
        print 'Samples free: %d' % status['samples_free']
        print 'Stop seconds: %.3f' % status['stop_seconds']
        print 'time: %s' % status['time']
    else:
        print 'Probe not found!'
