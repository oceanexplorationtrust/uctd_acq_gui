#!/usr/bin/python

# Copyright 2016 Ocean Exploration Trust & Ian Vaughn
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import NGcore.NGtime
import datetime

import urllib
import urllib2
import json

class NavServeException(Exception):
    pass

class Client(object):
    def __init__(self, addr, port=8080):
        self._addr = addr
        self._port = port
        self._base_url = 'http://%s:%d/' % (self._addr, self._port)

    def getNavByTime(self, vehicle, time):
        data = {'vehicle': vehicle}
        if isinstance(time, datetime.datetime):
            data['time'] = time.isoformat() + 'Z'
        else:
            data['time'] = time

        url_values = urllib.urlencode(data)
        url = self._base_url + 'navbytime/?' + url_values

        return  self._get(url)

    def getCruiseId(self):
        url = self._base_url + 'cruiseId'
        obj = self._get(url)
        return obj['cruiseId']

    def _get(self, url):
        #print url
        try:
            response = urllib2.urlopen(url)
        except Exception as e:
            print str(e)
            print('Failed with url: ' + url)

        obj = json.loads(response.read())
        if obj['ok']:
            return obj

        msg = obj['exception']
        msg_fmt = '\n'.join(['SERVER: %s' % line for line in msg.split('\n')])

        raise NavServeException(msg_fmt)



